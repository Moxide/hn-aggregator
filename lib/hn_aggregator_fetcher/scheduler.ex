defmodule HnAggregatorFetcher.Scheduler do
  @moduledoc """
  Handles the periodic fetching of top stories every 5 minutes

  On a successful fetching, stories are stored in HnAggregatorStorage and dispatched to
  websockets clients via HnAggregatorWeb

  If the HN API is down, the fetching will be retried every 10 seconds
  """

  # NOTES:
  # * The 5 minutes delay is not very precise and is relative to the last time that the API has been successfully accessed
  # * Actual implementation of the The API fetching and storage could have been merged in a single gen_statem
  # * Would be better to retry using an exponential backoff

  use GenServer
  require Logger

  @retrying_interval_in_milliseconds 10_000
  @fetching_interval_in_milliseconds 300_000

  def start_link(state) do
    GenServer.start_link(__MODULE__, state, name: __MODULE__)
  end

  @impl true
  def init(state) do
    {:ok, state, {:continue, :fetch_top_stories}}
  end

  @impl true
  @spec handle_continue(:fetch_top_stories, any) :: {:noreply, any, :hibernate}
  def handle_continue(:fetch_top_stories, state) do
    trigger_fetching()
    {:noreply, state, :hibernate}
  end

  @impl true
  def handle_info(:fetch_top_stories, state) do
    trigger_fetching()
    {:noreply, state, :hibernate}
  end

  defp trigger_fetching() do
    case fetch_and_store_top_stories() do
      :ok ->
        schedule_fetching(@fetching_interval_in_milliseconds)

        Logger.info(
          "Top stories fetched, next fetching in #{@fetching_interval_in_milliseconds} milliseconds"
        )

      {:error, message} ->
        schedule_fetching(@retrying_interval_in_milliseconds)

        Logger.error(
          "Error during top stories fetching with message #{message} retrying in #{@retrying_interval_in_milliseconds} milliseconds"
        )
    end
  end

  defp fetch_and_store_top_stories() do
    case HnAggregatorFetcher.HnApi.fetch_top_stories() do
      {:ok, stories} ->
        HnAggregatorStorage.store_top_stories(stories)
        HnAggregatorWeb.Endpoint.broadcast("story", "new_stories", %{stories: stories})
        :ok

      {:error, _message} = error ->
        error
    end
  end

  defp schedule_fetching(delay_in_milliseconds) do
    Process.send_after(__MODULE__, :fetch_top_stories, delay_in_milliseconds)
  end
end
