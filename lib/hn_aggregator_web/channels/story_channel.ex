defmodule HnAggregatorWeb.StoryChannel do
  @moduledoc false
  use HnAggregatorWeb, :channel

  def join("story", _params, socket) do
    {:ok, stories} = HnAggregatorStorage.fetch_top_stories()

    {:ok, Map.from_struct(stories), socket}
  end
end
