defmodule HnAggregatorWeb.StoryChannelTest do
  use HnAggregatorWeb.ChannelCase

  alias HnAggregatorWeb.{UserSocket, StoryChannel}

  setup do
    {:ok, _, socket} =
      UserSocket
      |> socket("user_id", %{})
      |> subscribe_and_join(StoryChannel, "story")
      %{socket: socket}
  end

  # Not meaningful yet
  test "send new stories to all users", %{socket: sokcet} do
    stories = [%{a: :b, b: :c}]
    HnAggregatorWeb.Endpoint.broadcast("story", "new_stories", stories)
    assert_push "new_stories", stories
  end

end
