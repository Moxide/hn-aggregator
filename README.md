# HnAggregator

## Quick start

### Prerequisites

* Elixir 1.11+
* an internet connection for HN new fetching
* local port 4000 must be available
### How to run

In the repository folder, run `iex -S mix phx.server`

### Testing HTTP API

Using an HTTP client or within a web browser, you can access the following Urls :

* [Full stories](http://localhost:4000/api/topstories)
* [Pagination](http://localhost:4000/api/topstories?page=3)
* [Individual story](http://localhost:4000/api/story/25829748)

## Testing websockets

Within iex, run `WebsocketsClient.run()`.
It will start a Phoenix client, join the `story` channel, and returns the on-join payload.

Broadcasted messages to these channels can be observed in the iex process message box.

## Notes for review

### Resilience

If the HN API is down, the storage and web layers are still available.
The last fetched date is given in responses, so the client may act accordingly

If the HN API is already down on application start, the scheduler will enter its retry cycle, the stored stories being empty

### Performance

As stated in storage code source, GenServer implementation is limited by the process message handling throughput (ETS is best)

The public API performance is the Phoenix framework one.
If we need more simultaneous connections, we can raise the number of Cowboy pool workers.
We may scale horizontally (form a cluster)
### Clarity

The scheduler and storage genservers are started under the main application supervisor

### Security

The API is not secured (public)
If we want to make it private, we have to use https and an authentication/authorization method (JWT for instance)
Of course, it should be rate limited (either in Phoenix, or upfront)
### Dependencies

I used Phoenix only because of the Websockets handling.

## Final comments

To be honest, I'm in a rush !

### Testing
The testing part is obviously not enough.

* I usually use Hammox (hence the use of behaviours for components API)
* When a dependency has to be mocked, I either use dependency injection (though function options), or the following :

```
  defp impl do
    Application.get_env(:hn_aggregator_storage, :storage_module, HnAggregatorStorage.GenServer.Impl)
  end
```

and then, for instance

```
  @impl true
  def store_top_stories(stories) do
     case validate_top_stories(stories) do
      :ok -> impl().store_top_stories(stories)
      {:error, _message} = error -> error
     end
  end
```

This way we can switch implementation during tests

About GenServers, I invoke directly the callbacks, which may be not very good.
To test the timing (retry etc), we may have to modify genserver (make a "tick" public method ?)




