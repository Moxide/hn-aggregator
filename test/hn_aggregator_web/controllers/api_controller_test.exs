defmodule HnAggregatorWeb.APIControllerTest do
  use HnAggregatorWeb.ConnCase

  test "GET /", %{conn: conn} do
    conn = get(conn, "/api/topstories")
    response = json_response(conn, 200)
    assert is_map(response)
    assert Map.has_key?(response, "stories")
  end
end
