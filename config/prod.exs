use Mix.Config

config :hn_aggregator, HnAggregatorWeb.Endpoint,
  url: [host: "example.com", port: 80]

config :logger, level: :info

import_config "prod.secret.exs"
