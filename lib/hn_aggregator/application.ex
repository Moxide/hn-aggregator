defmodule HnAggregator.Application do
  @moduledoc false

  use Application

  def start(_type, _args) do
    children = [

      # Start the storage Genserver implementation
      %{
        id: HnAggregatorStorage.GenServer.Impl,
        start: {HnAggregatorStorage.GenServer.Impl, :start_link, [[]]}
      },
      # Start the fetcher scheduler
      %{
        id: HnAggregatorFetcher.Scheduler,
        start: {HnAggregatorFetcher.Scheduler, :start_link, [[]]}
      },
      # Start the PubSub system
      {Phoenix.PubSub, name: HnAggregator.PubSub},
      # Start the Endpoint (http/https)
      HnAggregatorWeb.Endpoint
      # Start a worker by calling: HnAggregator.Worker.start_link(arg)
      # {HnAggregator.Worker, arg}
    ]

    opts = [strategy: :one_for_one, name: HnAggregator.Supervisor]
    Supervisor.start_link(children, opts)
  end

  def config_change(changed, _new, removed) do
    HnAggregatorWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
