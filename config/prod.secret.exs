use Mix.Config

config :hn_aggregator, HnAggregatorWeb.Endpoint,
  http: [
    port: String.to_integer(System.get_env("PORT") || "4000"),
    transport_options: [socket_opts: [:inet6]],
    server: true
  ]
