defmodule HnAggregatorStorage.Data do

  defstruct [:last_fetched_date, :stories]


  @typedoc "A stored data structure"
  @type t() :: %__MODULE__{
    last_fetched_date: DateTime.t(),
    stories: list(map())
  }

end
