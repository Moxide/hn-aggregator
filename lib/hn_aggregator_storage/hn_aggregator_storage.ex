defmodule HnAggregatorStorage do
  @moduledoc """
  Handles the top stories in-memory storage
  """

  @behaviour HnAggregatorStorage.Behaviour
  @storage_genserver_impl HnAggregatorStorage.GenServer.Impl

  @doc """
  Stores the HN top stories
  Expects any list of maps as a parameter
  """
  @impl true
  def store_top_stories(stories) do
     case validate_top_stories(stories) do
      :ok -> @storage_genserver_impl.store_top_stories(stories)
      {:error, _message} = error -> error
     end
  end

  @doc """
  Fetches the HN top stories
  When no option is given, a full list of 50 stories is returned

  Options:
  * The `page` option is an integer between 1 and 5. A list of the corresponding 10 stories is returned.
  """
  @impl true
  def fetch_top_stories(options \\ []) do
    case validate_fetch_top_stories_options(options) do
      :ok -> @storage_genserver_impl.fetch_top_stories(options)
      {:error, _message} = error -> error
     end
  end

  @doc """
  Fetches a single HN story, given its ID
  """
  @impl true
  def fetch_top_story(story_id) do
    case validate_fetch_top_story_id(story_id) do
      :ok -> @storage_genserver_impl.fetch_top_story(story_id)
      {:error, _message} = error -> error
     end
  end

  # Basic validations, could be put in a separate module to make them testable in isolation
  # NimbleOptions library is also a lightweight option
  defp validate_top_stories(stories) when is_list(stories) do
    stories
    |> Enum.all?(&is_map/1)
    |> case do
      true -> :ok
      _ -> {:error, "top stories should be a list of maps"}
    end
  end

  defp validate_top_stories(_stories), do: {:error, "top stories should be a list of maps"}

  defp validate_fetch_top_stories_options(options) when is_list(options) do
     case Keyword.get(options, :page) do
      nil -> :ok
      page when is_integer(page) and page in 1..5 -> :ok
      _ -> {:error, "when specified, :page option should be an integer between 1 and 5"}
     end
  end

  defp validate_fetch_top_story_id(story_id) when is_integer(story_id), do: :ok
  defp validate_fetch_top_story_id(_), do: {:error, "Story ID should be an integer"}

end
