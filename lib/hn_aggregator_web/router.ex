defmodule HnAggregatorWeb.Router do
  use HnAggregatorWeb, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/api", HnAggregatorWeb do
    pipe_through :api

    get "/topstories", TopStoriesController, :index
    get "/story/:id", TopStoriesController, :show
  end

end
