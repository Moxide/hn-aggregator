defmodule HnAggregatorStorage.GenServer.Impl do
  @moduledoc false

  # NOTE:
  # GenServer implementatin is simple, but the bottleneck is the message throughput of the process
  # On a single node, direct access to ETS would be way more performant
  # On multiple nodes, a distributed store (Mnesia, or PubSub based store), or external Database such as Redis

  @behaviour HnAggregatorStorage.Behaviour
  use GenServer
  alias HnAggregatorStorage.Data

  def start_link(stories) do
    GenServer.start_link(__MODULE__, stories, name: __MODULE__)
  end

  @impl true
  def init(stories) do
    {:ok, %Data{last_fetched_date: DateTime.utc_now(), stories: stories}}
  end

  @impl true
  def store_top_stories(stories) do
    GenServer.call(__MODULE__, {:store_top_stories, stories})
    :ok
  end

  @impl true
  def fetch_top_stories(options) do
    GenServer.call(__MODULE__, {:fetch_top_stories, options})
  end

  @impl true
  def fetch_top_story(story_id) do
    GenServer.call(__MODULE__, {:fetch_top_story, story_id})
  end


  @impl true
  def handle_call({:store_top_stories, stories}, _from, _state) do
    {:reply, :ok, %Data{last_fetched_date: DateTime.utc_now(), stories: stories}}
  end

  def handle_call({:fetch_top_stories, options}, _from, state) do
    page = Keyword.get(options, :page)
    data_to_return = state
    |> Map.put(:stories, records_to_return(state.stories, page))
    {:reply, {:ok, data_to_return}, state}
  end

  def handle_call({:fetch_top_story, story_id}, _from, state) do
    {:reply, single_story_response(state, story_id), state}
  end

  defp records_to_return(state, nil), do: state
  defp records_to_return(state, page) do
    low_index = 10 * (page - 1)
    high_index = 10 * page
    state
    |> Enum.slice(low_index..high_index)
  end

  defp single_story_response(state, story_id) do
    state
    |> Map.get(:stories)
    |> Enum.find(&(&1["id"] == story_id))
    |> case do
      nil -> {:error, "Story #{story_id} not found"}
      story -> {:ok, story}
    end
  end

end
