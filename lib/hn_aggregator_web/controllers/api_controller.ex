defmodule HnAggregatorWeb.TopStoriesController do
  use HnAggregatorWeb, :controller

  def index(conn, params) do
    options = if Map.has_key?(params, "page") do
      [page: String.to_integer(params["page"])]
    else
      []
    end
    case HnAggregatorStorage.fetch_top_stories(options) do
      {:ok, stories} -> json(conn, Map.from_struct(stories))
      {:error, message} -> json(Plug.Conn.put_status(conn, 500), %{error: message})
    end
  end

  def show(conn, %{"id" => story_id_str}) do
    case HnAggregatorStorage.fetch_top_story(String.to_integer(story_id_str)) do
      {:ok, story} -> json(conn, story)
      {:error, message} -> json(Plug.Conn.put_status(conn, 404), %{error: message})
    end

  end
end
