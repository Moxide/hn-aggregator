defmodule HnAggregatorFetcher.HnApi do
  @moduledoc """
  Handles interactions with the HN API server
  """
  @behaviour HnAggregatorFetcher.HnApi.Behaviour

  @api_base_url Application.fetch_env!(:hn_aggregator, HnAggregatorFetcher.HnApi)
                |> Keyword.get(:base_url)
                |> String.to_charlist()

  @max_top_stories 50

  @doc """
  Fetches the HackerNews 50 top stories from HN API server
  It must be done in two phases:
  * first, retrieve the top stories Ids
  * then, fetch the full stories individually
  """
  @impl true
  def fetch_top_stories() do
    with {:ok, stories_ids} <- fetch_top_stories_ids(),
         :ok <- validate_stories_ids(stories_ids),
         {:ok, stories} <- fetch_top_stories(stories_ids) do
      {:ok, stories}
    else
      {:error, _message} = error -> error
    end
  end

  defp fetch_top_stories_ids() do
    top_stories_url = @api_base_url ++ '/topstories.json'
    http_get(top_stories_url)
  end

  defp fetch_top_stories(stories_ids) do
    # NOTE: Using supervised tasks would be more resilient, if an individual API call crash
    stories_list =
      stories_ids
      |> Enum.take(@max_top_stories)
      |> Task.async_stream(&fetch_item/1)
      |> Enum.to_list()

    case Enum.all?(stories_list, fn
           {:ok, {:ok, _}} -> true
           _ -> false
         end) do
      true -> {:ok, Enum.map(stories_list, fn {:ok, {:ok, story}} -> story end)}
      false -> {:error, "Could fetch all stories"}
    end
  end

  defp fetch_item(item_id) do
    item_url = @api_base_url ++ '/item/' ++ Integer.to_charlist(item_id) ++ '.json'
    http_get(item_url)
  end

  defp validate_stories_ids(stories_ids) when is_list(stories_ids) do
    case Enum.all?(stories_ids, &is_integer/1) do
      true -> :ok
      _ -> {:error, "Top stories ids should be a list of integers"}
    end
  end

  defp validate_stories_ids(_), do: {:error, "Top stories ids should be a list of integers"}

  defp http_get(url) do
    case :httpc.request(url) do
      {:ok, {{_, 200, _}, _, response}} ->
        decode_response(response)

      {:ok, {{_, error_code, _}, _, response}} ->
        {:error, "API error code: #{inspect(error_code)}, response: #{inspect(response)}"}

      _ ->
        {:error, "API Connect error"}
    end
  end

  defp decode_response(response) when is_binary(response) or is_list(response),
    do: Jason.decode(response)

  defp decode_response(_), do: {:error, "Could not decode API response"}
end
