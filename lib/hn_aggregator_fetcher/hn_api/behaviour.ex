defmodule HnAggregatorFetcher.HnApi.Behaviour do
   @moduledoc false
   @callback fetch_top_stories() :: {:ok, list(integer())} | {:error, String.t()}
end
