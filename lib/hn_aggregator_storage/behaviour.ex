defmodule HnAggregatorStorage.Behaviour do
  @moduledoc false
  alias HnAggregatorStorage.Data

  @type fetch_top_stories_option :: {:page, integer()}

  @callback store_top_stories(list(map())) :: :ok | {:error, String.t()}
  @callback fetch_top_stories([fetch_top_stories_option]) :: {:ok, Data.t()} | {:error, String.t()}
  @callback fetch_top_story(integer()) :: {:ok, map()} | {:error, String.t() | :not_found}

end
