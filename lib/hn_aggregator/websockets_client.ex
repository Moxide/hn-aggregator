defmodule WebsocketsClient do
  @moduledoc """
  Helper with simulate a Phoenix websockets JS client
  """

  @doc """
  Starts a websocket client, and joins the "story" channel

  Returns the on-join payload
  Broadcasted messages to the "story" channel can be seen in the caller process message box (use flush())
  """
  def run() do
    socket_opts = [
      url: "ws://localhost:4000/socket/websocket"
    ]

    {:ok, socket} = PhoenixClient.Socket.start_link(socket_opts)
    wait_until_connected(socket)
    
    {:ok, response, _channel} = PhoenixClient.Channel.join(socket, "story")
    response
  end

  defp wait_until_connected(socket) do
    if !PhoenixClient.Socket.connected?(socket) do
      Process.sleep(100)
      wait_until_connected(socket)
    end
  end
end
