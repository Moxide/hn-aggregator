use Mix.Config

config :hn_aggregator, HnAggregatorWeb.Endpoint,
  http: [port: 4002],
  server: false

config :logger, level: :warn
