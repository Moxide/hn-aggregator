defmodule HnAggregatorWeb.Endpoint do
  use Phoenix.Endpoint, otp_app: :hn_aggregator

  socket "/socket", HnAggregatorWeb.UserSocket,
    websocket: true,
    longpoll: false

  plug Plug.Parsers,
    parsers: [:json],
    pass: ["*/*"],
    json_decoder: Phoenix.json_library()

  plug Plug.MethodOverride
  plug Plug.Head
  plug HnAggregatorWeb.Router
end
