defmodule HnAggregatorStorage.GenServer.Test do
  use ExUnit.Case, async: true

  @storage_genserver HnAggregatorStorage.GenServer.Impl
  alias HnAggregatorStorage.Data

  describe "Storage GenServer implementation" do

    test "Storage should be initialized with an empty Data struct" do
      start_supervised(@storage_genserver)

      {:ok, %Data{} = data} = HnAggregatorStorage.fetch_top_stories()
      assert data.stories == []
    end

    test "A list of maps should be stored in the state" do

      list_of_maps = 1..10
      |> Enum.map(&(%{&1 => :test}))

      data = %Data{
        last_fetched_date: DateTime.utc_now(),
        stories: list_of_maps
      }

      response = HnAggregatorStorage.GenServer.Impl.handle_call({:store_top_stories, data}, nil, list_of_maps)

      assert {:reply, :ok, list_of_maps} = response

    end

    test "A full list of maps should be returned when no option is given" do

      list_of_maps = 1..50
      |> Enum.map(&(%{&1 => :test}))

      data = %Data{
        last_fetched_date: DateTime.utc_now(),
        stories: list_of_maps
      }

      response = HnAggregatorStorage.GenServer.Impl.handle_call({:fetch_top_stories, []}, nil, data)

      assert {:reply, {:ok, data}, data} = response

    end

    test "A page should be retrieved from a :page option is given" do

      page_number = 3
      low_index = 10 * (page_number - 1)
      high_index = 10 * page_number

      list_of_maps = 1..50 |> Enum.map(&(%{&1 => :test}))

      full_data = %Data{
        last_fetched_date: DateTime.utc_now(),
        stories: list_of_maps
      }

      page = list_of_maps |> Enum.slice(low_index..high_index)

      page_data = full_data
      |> Map.put(:stories, page)

      response = HnAggregatorStorage.GenServer.Impl.handle_call({:fetch_top_stories, [page: page_number]}, nil, full_data)

      assert {:reply, {:ok, page_data}, full_data} == response

    end

    # TODO: Add test cases

  end

end
