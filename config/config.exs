use Mix.Config

config :hn_aggregator, HnAggregatorWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "YqaffoEruSB/VjXvh5RyGzB6kMg3ty4IFRTlDpndGW4znkKrKpebWQG8mYuuwfsK",
  pubsub_server: HnAggregator.PubSub

config :hn_aggregator, HnAggregatorFetcher.HnApi,
  base_url: "https://hacker-news.firebaseio.com/v0"

config :logger, :console,
  format: "$time $metadata[$level] $message\n"

config :phoenix, :json_library, Jason

import_config "#{Mix.env()}.exs"
